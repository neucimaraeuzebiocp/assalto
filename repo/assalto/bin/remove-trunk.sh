#!/bin/sh

#
# @author Neucimara Euzebio <neucimaraeuzebiocp@gmail.com>
#


#
# Roteiro permite correr caminhos e passar todos os arquivos de trunk  
# para o caminho raiz.
#

#
# Criar caminho .assalto para esconderijos caso não exista.
#
[ -d ~/.assalto ] || mkdir --verbose ~/.assalto

#
# Criar caminho backup para gravar pastas em .assalto
#
[ -d ~/.assalto/backup ] || mkdir --verbose ~/.assalto/backup

#
# Criar o arquivo cicle.txt caso não exista. O arquivo cicle.txt é
# uma lista com todos os componentes existentes.
#
[ -f ~/.assalto/cicle.txt ] || touch ~/.assalto/cicle.txt

#
# Criar o script para fazer a execução.
#
[ -f ~/.assalto/kids.sh ] || touch ~/.assalto/kids.sh

#
# Esse objeto grava um caminho aonde o sistema deve fazer a leitura e gravação.
#
target_dir=""


#
# Obter um caminho alvo.
#
read -p "Caminho (Padrão Unix): " target_dir

#
# Gravar lista de caminhos em arquivo cicle.txt
#
/bin/python3 ../src/write_target_list.py $target_dir

#
# Obtem o nome do próximo pacote.
#
/bin/python3 ../src/cat/make.py $target_dir

#
# Executar lista de ordems gerada pelo sistema.
#
/bin/sh ~/.assalto/kids.sh
