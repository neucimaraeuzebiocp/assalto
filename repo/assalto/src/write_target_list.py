import sys

import os
import os.path

from os import listdir
from os.path import isfile
from os.path import join

#
# @author Neucimara Euzebio <neucimaraeuzebiocp@gmail.com>
#

#
# Suporte no telefone, tablet e tv.
#
# Argumentos:
#     0 ~> Caminho absoluto até caminho da pessoa.
#     1 ~> Caminho relativo, até caminho alvo.
#
args_list = sys.argv

#
# Caminho absoluto até alvo.
#
abs_path = os.path.expanduser(args_list[1])

#
# Correr a arvore.
#
for pos in listdir(abs_path):
    print("Gravando nome do caminho {0} em ~/.assalto/cicle.txt\n".format(pos))

    #
    # Gravando no final do arquivo.
    #
    f = open(os.path.expanduser("~/.assalto/cicle.txt"), "a")
    f.write("{0}\n".format(pos))
    f.close()
